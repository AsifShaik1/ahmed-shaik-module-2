void main() {
  // Q3.: Create a class and a) then use an object to print the name of the app,
  // sector/category, developer, and the year it won MTN Business App of
  // the Year Awards. b) Create a function inside the class, transform the
  // app name to all capital letters and then print the output.

  //a. For 2021
  MTNAppWinner Winner2021 = new MTNAppWinner(
      "Ambani", "Best Gaming Solution", "Ambani Africa", "2021");
  ;
  Winner2021.details();

  print("");

  //b. App name in all caps
  Winner2021.nameAllCaps();
}

class MTNAppWinner {
  String appName;
  String category;
  String developer;
  String year;

  MTNAppWinner(this.appName, this.category, this.developer, this.year) {}

  void init(aName, aCat, aDev, aYear) {
    this.appName = aName;
    this.category = aCat;
    this.developer = aDev;
    this.year = aYear;
  }

  void details() {
    print("App Name: $appName");
    print("Category: $category");
    print("Developer: $developer");
    print("Year: $year");
  }

  void nameAllCaps() {
    String? tmpStr;
    tmpStr = this.appName;
    this.appName = tmpStr.toUpperCase();
    print(this.appName);
  }
}
