void main() {
  // Q2.: Create an array to store all the winning apps of the MTN Business
  // App of the Year Awards since 2012;
  //    a) Sort and print the apps by name;
  //    b) Print the winning app of 2017 and the winning app of 2018.;
  //    c) the Print total number of apps from the array.

  var winners = {
    2012: "FNB",
    2013: "Snapscan",
    2014: "Live Inspect",
    2015: "WumDrop",
    2016: "Domestly",
    2017: "Shyft",
    2018: "Khula Ecosystem",
    2019: "Naked Insurance",
    2020: "EasyEquities",
    2021: "Ambani Africa"
  };
  winners.forEach((key, value) {
    print("The winner for $key was $value.");
  });

  //b.
  print("");
  print(winners[2017].toString() + " and " + winners[2018].toString() + ".");

  //c.
  print("");
  print(winners.length);
}
