void main() {
  // Q1.: Write a basic program that stores and then prints the following data:
  // Your name, favorite app, and city;
  
  String name;
  name = "Ahmed Asif Shaik";
  String fav_app;
  fav_app = "Duolingo";
  String city;
  city = "Pretoria";
  
  print('Name: $name');
  print('Favourite App: $fav_app');
  print('City: $city');
}
